# ViChat #

In order to increase my programming experience I've started to create my first server&client app what became ViChat. Now when it's working I'm planning to develop it so that I'll get much more experience.

### ViServer ###

* Listening for clients
* When client is joining then create new object to manage his actions
* 2 Threads per client

### ViClient ###

* GUI Application
* Connecting with server on app startup
* Login with name to server
* Individual thread for receiving

### ViData ###

* There's Packet structure with serializing
* Tools which can help a lot at client and server side
* Database connection for future clients managing (ie. registration, login)

### Note ###

Please notice that it's my first that big project and I'm trying to develop my skills to higher level. If you've found some bugs or know better solutions than I'll be thankful if you'll share it with me.